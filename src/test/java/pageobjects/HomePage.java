package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    private WebDriver driver;

    @FindBy(xpath = "//div[@id='contact-link']/a")
    private WebElement contactLink;

    public HomePage(WebDriver driver) {
        this.driver = driver;

        PageFactory.initElements(driver, this);
    }

    public String getContactLinkText() {
        return contactLink.getText();
    }

    public ContactFormPage clickContactLink() {
        contactLink.click();
        return new ContactFormPage(driver);
    }
}
